import React from 'react';
import {Route, Switch,BrowserRouter} from 'react-router-dom';
import testA from './components/testA';
import testB from './components/testB';
import App from './App';



const BasicRoute = () => (
    <BrowserRouter  >
        <Switch>
            <Route exact path='/' component = {App}/>
            <Route exact path="/testA" component={testA}/>
            <Route exact path="/testB/:id" component={testB}/>
            <Route exact path="/testB/:id?" component={testB}/>
        </Switch>
    </BrowserRouter>
);


export default BasicRoute;
