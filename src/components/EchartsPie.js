import React, { Component } from 'react';

// 引入 ECharts 主模块
import echarts from 'echarts/lib/echarts';
// 引入柱状图
import  'echarts/lib/chart/pie';
// 引入提示框和标题组件
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';

class EchartsPie extends Component {
    componentDidMount() {
        function genData(count) {
            var nameList = [
                '赵', '钱', '孙', '李', '周', '吴', '郑', '王', '冯', '陈', '褚', '卫', '蒋', '沈', '韩', '杨', '朱', '秦', '尤', '许', '何', '吕', '施', '张', '孔', '曹', '严', '华', '金', '魏', '陶', '姜', '戚', '谢', '邹', '喻', '柏', '水', '窦', '章', '云', '苏', '潘', '葛', '奚', '范', '彭', '郎', '鲁', '韦', '昌', '马', '苗', '凤', '花', '方', '俞', '任', '袁', '柳', '酆', '鲍', '史', '唐', '费', '廉', '岑', '薛', '雷', '贺', '倪', '汤', '滕', '殷', '罗', '毕', '郝', '邬', '安', '常', '乐', '于', '时', '傅', '皮', '卞', '齐', '康', '伍', '余', '元', '卜', '顾', '孟', '平', '黄', '和', '穆', '萧', '尹', '姚', '邵', '湛', '汪', '祁', '毛', '禹', '狄', '米', '贝', '明', '臧', '计', '伏', '成', '戴', '谈', '宋', '茅', '庞', '熊', '纪', '舒', '屈', '项', '祝', '董', '梁', '杜', '阮', '蓝', '闵', '席', '季', '麻', '强', '贾', '路', '娄', '危'
            ];
            var legendData = [];
            var seriesData = [];
            var selected = {};
            for (var i = 0; i < 50; i++) {
                var name = Math.random() > 0.65
                    ? makeWord(4, 1) + '·' + makeWord(3, 0)
                    : makeWord(2, 1);
                legendData.push(name);
                seriesData.push({
                    name: name,
                    value: Math.round(Math.random() * 100000)
                });
                selected[name] = i < 6;
            }

            return {
                legendData: legendData,
                seriesData: seriesData,
                selected: selected
            };

            function makeWord(max, min) {
                var nameLen = Math.ceil(Math.random() * max + min);
                var name = [];
                for (var i = 0; i < nameLen; i++) {
                    name.push(nameList[Math.round(Math.random() * nameList.length - 1)]);
                }
                return name.join('');
            }
        }
        var data = genData(50);
        console.log(data)
        // var option = {
        //     title : {
        //         text: '同名数量统计',
        //         subtext: '纯属虚构',
        //         x:'center'
        //     },
        //     tooltip : {
        //         trigger: 'item',
        //         formatter: "{a} <br/>{b} : {c} ({d}%)"
        //     },
        //     legend: {
        //         type: 'scroll',
        //         orient: 'vertical',
        //         right: 10,
        //         top: 20,
        //         bottom: 20,
        //         data: data.legendData,
        //
        //         selected: data.selected
        //     },
        //     series : [
        //         {
        //             name: '姓名',
        //             type: 'pie',
        //             radius : '55%',
        //             center: ['40%', '50%'],
        //             data: data.seriesData,
        //             itemStyle: {
        //                 emphasis: {
        //                     shadowBlur: 10,
        //                     shadowOffsetX: 0,
        //                     shadowColor: 'rgba(0, 0, 0, 0.5)'
        //                 }
        //             }
        //         }
        //     ]
        // };
        const optionPie = {
            legend: {

                right: 'right',
                bottom: '50%',
                data: ['Matcha Latte', 'Milk Tea', 'Cheese Cocoa', 'Walnut Brownie']
            },
            dataset: {
                source: [
                    ['Matcha Latte', 41.1, 30.4, 65.1, 53.3, 83.8, 98.7],
                    ['Milk Tea', 86.5, 92.1, 85.7, 83.1, 73.4, 55.1],
                    ['Cheese Cocoa', 24.1, 67.2, 79.5, 86.4, 65.2, 82.5],
                    ['Walnut Brownie', 55.2, 67.1, 69.2, 72.4, 53.9, 39.1]

                ]
            },
            series: [
                {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['25%', '25%']
                    // No encode specified, by default, it is '2012'.
                },
                {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['50%', '25%']
                    // No encode specified, by default, it is '2012'.
                }, {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['75%', '25%'],

                },
                {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['25%', '75%']
                    // No encode specified, by default, it is '2012'.
                }, {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['50%', '75%'],

                }, {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['75%', '75%'],
                }]

        }
        var option = {
            legend: {

                right: 'right',
                bottom: '50%',
                data: ['Matcha Latte', 'Milk Tea', 'Cheese Cocoa', 'Walnut Brownie']
            },
            toolbox: {
                show: true,
                feature: {
                    magicType: { type: ['bar'] },
                }
            },

            dataset: {
                source: [
                    ['Matcha Latte', 41.1, 30.4, 65.1, 53.3, 83.8, 98.7],
                    ['Milk Tea', 86.5, 92.1, 85.7, 83.1, 73.4, 55.1],
                    ['Cheese Cocoa', 24.1, 67.2, 79.5, 86.4, 65.2, 82.5],
                    ['Walnut Brownie', 55.2, 67.1, 69.2, 72.4, 53.9, 39.1]
                ]
            },
            series: [
                {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['25%', '25%']
                    // No encode specified, by default, it is '2012'.
                },
                {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['50%', '25%']
                    // No encode specified, by default, it is '2012'.
                }, {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['75%', '25%'],

                },
                {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['25%', '75%']
                    // No encode specified, by default, it is '2012'.
                }, {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['50%', '75%'],

                }, {
                    type: 'pie',
                    radius: [0, '20%'],
                    center: ['75%', '75%'],
                }]
        }

        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById("container"));
        // 绘制图表
        // myChart.setOption(optionPie);
        if (option && typeof option === "object") {
            myChart.setOption(optionPie, true);
        }



    }
    render() {
        return (
            <div id="container" style={{ height: '100%'}}></div>
            // <div id="hehe" style={{ width: 400, height: 400 }}></div>
        );
    }
}

export { EchartsPie};
//export default EchartsTest;不行
