import React from 'react';
import {Button} from 'antd'
import {EchartsTest} from './Echartstest'
import {EchartsPie} from './EchartsPie'


export default class TestB extends React.Component {
    constructor(props) {
        super(props);

        this.state = {userID: ''};
    }

    componentDidMount() {
        console.log(123, this.props.match.params);
        this.setState({userID: this.props.match.params.id})
        console.log(this.props.history)
    }

    render() {
        return (
            <div>
                <h4>这是B界面</h4>
                <a href='/testA'>去A</a>

                <h1>用户ID：{this.state.userID}</h1>

                <Button type="primary" onClick={() => this.props.history.push(
                    {pathname: '/testA', state: {uname: 'hehe', uid: 3}}
                )}>函数跳转</Button>

                <Button onClick={() => this.props.history.goBack()}>上一页</Button>

                <EchartsTest name='大何向东流'/>
                <EchartsPie/>

            </div>
        )
    }
}
