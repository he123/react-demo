import React from 'react';



export default class TestA extends React.Component {
    constructor(props){
        super(props);
        this.state = {user:{}}
    }
    componentDidMount() {
        var user = this.props.history.location.state
        console.log(user)
        if(user){
            this.setState({user:user})
        }

    }

    render() {
        if(this.state.user.uid){
            return (
                <div>
                    <h4>这是A界面</h4>

                    <a href='/testB/3'>去B</a>
                    <a href='/testB'>去B,不加：id</a>

                    <button onClick={() => this.props.history.push('/')}>回到主页</button>
                    if( this.state.user !== {}){
                    <h2>B隐式传参过来的：<br/>名字：{this.state.user.uname}<br/>id:{this.state.user.uid}</h2>
                }

                </div>
            )
        }

        return (
            <div>
                <h4>这是A界面</h4>
                <a href='/testB/3'>去B</a>

            </div>
        )
    }
}
