const {override, fixBabelImports} = require('customize-cra');

module.exports = override(
    // antd 按需引用
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: 'css',
    }),

);
